import asyncio
import websockets
from settings import interface, port

async def hello():
    uri = "ws://"+ interface + ":" + str(port)
    async with websockets.connect(uri) as websocket:
        await websocket.send("Hello world!")
        msg = await websocket.recv()
        print(str(msg))

asyncio.get_event_loop().run_until_complete(hello())