import asyncio
import websockets
from settings import interface, port

async def echo(websocket, path):
    async for message in websocket:
        print("RCVD < " + str(message))
        await websocket.send(message)
        print("SENT > " + str(message))

start_server = websockets.serve(echo, interface, port)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()