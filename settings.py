import os

interface = os.getenv("WS_INTERFACE", "127.0.0.1")
port = os.getenv("WS_PORT", "1883")