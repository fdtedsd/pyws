Python Websocket Example
========================

Instalation
-----------
Requires a Python environment running Python 3.7.1+.

Run:
pip install requirements.txt

Servers
-------

There are two servers avaiable for testing purposes, the echo server (server.py) and the random server(random_server.py). Both use the following env variables for configuration

*WS_INTERFACE: The network interfaces to be bound for listeing server (defaults to 127.0.0.1).
*WS_PORT: The bound port (defaults to 1883).

Both can be run by simply using:

python selected_server_file

The echo server operates as a traditional TTY/TELNET echo and also outputs INPUT and OUTPUT streams to STDOUT using RCVD and SENT prefixes.

The random_server streams ISO DATETIME strings on random intervals.


Clients
-------
The basic client connects to the server and sends a hello world. It's a python CLI and supports the same WS_INTERFACE and WS_PORT env variables.

One can run it by typing:

python client.py

There's also a html client written in plain JS which allows the connection to the WS and displays the received messages. One may serve the html file using the python built-in simple http server by running:

python -m http.server 8000

inside the directory one intends to serve.
